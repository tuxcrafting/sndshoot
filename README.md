# sndshooter

sndshooter is a (proof-of-concept) raycasted 3D engine that renders
the player view as sound, thus allowing the world to be navigated
without having to see the screen.

It works by mapping the different "pixels" of the screen to different
frequencies, the center being at 200 Hz with the frequency being
multiplied by 1.15 for each pixel further away. Pixels to the left of
the center are sent to the left channel, to the right are sent to the
right channel.

It could probably be made more usable by tweaking the parameters that
go in the sound rendering.

The only dependencies are SDL2 and a D compiler.
