import derelict.sdl2.sdl;

import gamestate;
import graphdisp;
import mainloop;
import sounddisp;

void main() {
	DerelictSDL2.load();
	GameState state;
	Mainloop mainloop = Mainloop(state);
	GraphDisp graphdisp = GraphDisp(state);
	SoundDisp sounddisp = SoundDisp(state);
	mainloop.start();
	for (;;) {
		SDL_Event ev;
		mainloop.getEvent(ev);
		const s = state.handle(ev);
		const g = graphdisp.handle(ev);
		const d = sounddisp.handle(ev);
		if (!s || !g || !d) {
			break;
		}
	}
	SDL_Quit();
}
