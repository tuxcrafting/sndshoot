module graphdisp;

import derelict.sdl2.sdl;

import exceptions;
import gamestate;

struct GraphDisp {
private:
	GameState* m_state;
	SDL_Window* m_win;
	SDL_Renderer* m_renderer;
	SDL_Texture* m_texture;
	const width = 51*8;
	const height = 260;
public:
	this(ref GameState state) {
		m_state = &state;
		if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0) {
			throw new SDLError("Video initialization failed");
		}
		m_win = SDL_CreateWindow("sndshoot",
		                         SDL_WINDOWPOS_UNDEFINED,
		                         SDL_WINDOWPOS_UNDEFINED,
		                         width, height,
		                         SDL_WINDOW_SHOWN);
		if (m_win == null) {
			throw new SDLError("Window creation failed");
		}
		m_renderer = SDL_CreateRenderer(m_win, -1, 0);
		if (m_renderer == null) {
			throw new SDLError("Renderer creation failed");
		}
		m_texture = SDL_CreateTexture(m_renderer,
		                              SDL_PIXELFORMAT_RGBA8888,
		                              SDL_TEXTUREACCESS_STREAMING,
		                              width, height);
		if (m_texture == null) {
			throw new SDLError("Texture creation failed");
		}
	}

	void display() {
		void* pixels;
		int pitch;
		SDL_LockTexture(m_texture, null, &pixels, &pitch);

		for (size_t i = 25 * 32, j = 0;
		     j < 10; i += pitch, j++) {
			for (size_t xi = 0; xi < 32; xi += 4) {
				*cast(uint*)&pixels[i + xi] = 0xFF0000FF;
			}
		}

		const size_t base = pitch * 10;
		for (int vi = 0; vi < 51; vi++) {
			for (size_t i = base + vi * 32, j = 0;
			     j < 250; i += pitch, j++) {
				uint c = cast(uint)(m_state.view[vi] * 255);
				uint d = (c << 8) | (c << 16) | (c << 24)
					| 0x000000FF;
				for (size_t xi = 0; xi < 32; xi += 4) {
					*cast(uint*)&pixels[i + xi] = d;
				}
			}
		}

		SDL_UnlockTexture(m_texture);
		SDL_RenderCopy(m_renderer, m_texture, null, null);
	}

	bool handle(ref SDL_Event ev) {
		if (ev.type == SDL_QUIT) {
			return false;
		} else if (ev.type == SDL_USEREVENT) {
			SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
			SDL_RenderClear(m_renderer);
			display();
			SDL_RenderPresent(m_renderer);
		}
		return true;
	}

	~this() {
		SDL_DestroyTexture(m_texture);
		SDL_DestroyRenderer(m_renderer);
		SDL_DestroyWindow(m_win);
	}
}
