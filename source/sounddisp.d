module sounddisp;

import derelict.sdl2.sdl;
import std.algorithm;
import std.math;
import std.stdio;

import exceptions;
import gamestate;

struct SoundDisp {
private:
	GameState* m_state;
	SDL_AudioDeviceID m_dev;
	SDL_AudioSpec m_have;
	double time = 0.0;

	static int sineAt(double t, double hz, double amp)
	nothrow @nogc pure {
		return cast(int)(sin(t * hz) * amp * 32767);
	}

	static short clamp(int n) nothrow @nogc pure {
		return cast(short)(min(32767, max(-32768, n)));
	}

	extern(C)
	static void audioCb(void* userdata,
	                    ubyte* stream,
	                    int len) nothrow @nogc {
		SoundDisp* self = cast(SoundDisp*)userdata;
		assert(len % 4 == 0);
		for (int i = 0; i < len; i += 4) {
			short* l = cast(short*)&stream[i];
			short* r = cast(short*)&stream[i + 2];
			self.time += 2.0 * PI / cast(double)self.m_have.freq;

			int il = 0, ir = 0;

			double fq = 200.0;
			double amp = 0.5;
			for (int vi = 0; vi < 26; vi++) {
				il += sineAt(self.time, fq,
				             self.m_state.view[25 - vi] * amp);
				ir += sineAt(self.time, fq,
				             self.m_state.view[vi + 25] * amp);
				fq *= 1.15;
				amp /= 1.06;
			}

			*l = clamp(il);
			*r = clamp(ir);
		}
	}
public:
	this(ref GameState state) {
		m_state = &state;
		if (SDL_InitSubSystem(SDL_INIT_AUDIO) != 0) {
			throw new SDLError("Audio initialization failed");
		}
		SDL_AudioSpec want;
		want.freq = 44100;
		want.format = AUDIO_S16SYS;
		want.channels = 2;
		want.samples = 4096;
		want.callback = &audioCb;
		want.userdata = &this;
		m_dev = SDL_OpenAudioDevice(null, 0, &want, &m_have,
		                            SDL_AUDIO_ALLOW_FREQUENCY_CHANGE);
		if (m_dev == 0) {
			throw new SDLError("Failed to open audio device");
		}
		SDL_PauseAudioDevice(m_dev, 0);
	}

	bool handle(ref SDL_Event ev) {
		return true;
	}

	~this() {
		SDL_CloseAudioDevice(m_dev);
	}
}
