module mainloop;

import derelict.sdl2.sdl;

import exceptions;
import gamestate;

struct Mainloop {
private:
	GameState* m_state;

	extern(C)
	static uint timerCb(uint interval, void* param) nothrow @nogc {
		SDL_Event ev;
		ev.type = SDL_USEREVENT;
		ev.user.type = SDL_USEREVENT;
		ev.user.code = 0;
		ev.user.data1 = null;
		ev.user.data2 = null;
		SDL_PushEvent(&ev);
		return interval;
	}
public:
	this(ref GameState state) {
		m_state = &state;
		if (SDL_InitSubSystem(SDL_INIT_EVENTS) != 0) {
			throw new SDLError("Events initialization failed");
		}
		if (SDL_InitSubSystem(SDL_INIT_TIMER) != 0) {
			throw new SDLError("Timer initialization failed");
		}
	}

	void start() {
		SDL_AddTimer(1000 / 60, &timerCb, null);
	}

	void getEvent(out SDL_Event ev) {
		SDL_WaitEvent(&ev);
	}

	~this() {
		SDL_Quit();
	}
}
