module exceptions;

import derelict.sdl2.sdl;
import std.string;

class SDLError : Exception {
public:
	this(string msg) {
		super(cast(string)(msg ~ " - " ~
		                   SDL_GetError().fromStringz()));
	}
}
