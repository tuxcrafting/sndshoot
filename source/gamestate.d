module gamestate;

import derelict.sdl2.sdl;
import std.algorithm;
import std.math;
import std.stdio;

struct GameState {
private:
	string[] m_map = [
		"++++++++++++++++",
		"+..............+",
		"+..............+",
		"+...+..........+",
		"+...+..........+",
		"+...+..........+",
		"+..............+",
		"+......++......+",
		"+......++......+",
		"+..............+",
		"+..............+",
		"+...+....+.....+",
		"+...+....+++...+",
		"+...+......+...+",
		"+...+..........+",
		"++++++++++++++++"
	];
	const mspeed = 0.05;
	const rspeed = 0.05;
	double m_x = 2.0, m_y = 2.0;
	double m_dx = 1.0, m_dy = 0.0;
	double m_px = 0.0, m_py = 0.66;
public:
	double[51] view;

	bool handle_keys() {
		ubyte* keystate = SDL_GetKeyboardState(null);
		if (keystate[SDL_SCANCODE_ESCAPE]) {
			return false;
		}
		if (keystate[SDL_SCANCODE_UP]) {
			move(mspeed);
		}
		if (keystate[SDL_SCANCODE_DOWN]) {
			move(-mspeed);
		}
		if (keystate[SDL_SCANCODE_LEFT]) {
			turn(-rspeed);
		}
		if (keystate[SDL_SCANCODE_RIGHT]) {
			turn(rspeed);
		}
		return true;
	}

	void move(double d) {
		double nx = m_x + m_dx * d;
		double ny = m_y + m_dy * d;
		if (m_map[cast(int)ny][cast(int)nx] == '.') {
			m_x = nx;
			m_y = ny;
		}
	}

	void turn(double t) {
		double odx = m_dx;
		m_dx = m_dx * cos(t) - m_dy * sin(t);
		m_dy = odx * sin(t) + m_dy * cos(t);
		double opx = m_px;
		m_px = m_px * cos(t) - m_py * sin(t);
		m_py = opx * sin(t) + m_py * cos(t);
	}

	void render() {
		for (int i = 0; i < 51; i++) {
			double cx = 2.0 * cast(double)i / 51.0 - 1.0;
			double rdx = m_dx + m_px * cx;
			double rdy = m_dy + m_py * cx;
			int mx = cast(int)m_x;
			int my = cast(int)m_y;
			double ddx = abs(1.0 / rdx);
			double ddy = abs(1.0 / rdy);
			int sx, sy;
			double sdx, sdy;
			int side;
			if (rdx < 0.0) {
				sx = -1;
				sdx = (m_x - m_x.floor) * ddx;
			} else {
				sx = 1;
				sdx = (m_x.floor + 1.0 - m_x) * ddx;
			}
			if (rdy < 0.0) {
				sy = -1;
				sdy = (m_y - m_y.floor) * ddy;
			} else {
				sy = 1;
				sdy = (m_y.floor + 1.0 - m_y) * ddy;
			}
			for (;;) {
				if (sdx < sdy) {
					sdx += ddx;
					mx += sx;
					side = 0;
				} else {
					sdy += ddy;
					my += sy;
					side = 1;
				}
				if (m_map[my][mx] == '+') {
					break;
				}
			}
			double pwd;
			if (side == 0) {
				pwd = (cast(double)mx - m_x +
				       cast(double)(1 - sx) / 2.0) / rdx;
			} else {
				pwd = (cast(double)my - m_y +
				       cast(double)(1 - sy) / 2.0) / rdy;
			}
			view[i] = min(1.0, 1.0 / pwd);
		}
	}

	bool handle(ref SDL_Event ev) {
		if (ev.type == SDL_USEREVENT) {
			if (!handle_keys()) {
				return false;
			}
			render();
		}
		return true;
	}
}
